from sqlalchemy.engine import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm.session import Session


class DataMigration(object):

    @property
    def __name__(self):
        return self.__class__.__name__.lower()

    def __init__(self):
        self.engine = None
        self.session = None

    def setup(self, url):
        self.engine = create_engine(url)
        self.session = Session(self.engine)
        self.Base = automap_base()
        self.Base.prepare(self.engine, reflect=True)

    def finish(self):
        self.session.close()
        self.engine.dispose()

    def before(self):
        pass

    def after(self):
        pass
