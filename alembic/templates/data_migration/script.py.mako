"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}

from alembic import op
from alembic.data_migration import DataMigration
import sqlalchemy as sa
${imports if imports else ""}


@classmethod
def upgrade_helper(self):
    ${upgrades if upgrades else "pass"}


class Upgrade(DataMigration):

    def before(self):
        pass

    def after(self):
        pass


Upgrade.__call__ = upgrade_helper


@classmethod
def downgrade_helper(self):
    ${downgrades if downgrades else "pass"}


class Downgrade(DataMigration):

    def before(self):
        pass

    def after(self):
        pass


Downgrade.__call__ = downgrade_helper


upgrade = Upgrade()
downgrade = Downgrade()
